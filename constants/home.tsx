import {Product} from "@/services/models/Product";
import {Banner} from "@/services/models/Banner";
import {Call, Map, Send} from "@/components/common/svgIcons";
import {MetaProps} from "@/components/meta";

export const topBannerSlider: Banner[] = [
    {
        id: "1",
        image: "/images/image1.png",
        link: "#",
        alt: "images1"
    },{
        id: "2",
        image: "./images/image1.png",
        link: "#",
        alt: "images2"
    },{
        id: "3",
        image: "./images/image1.png",
        link: "#",
        alt: "images3"
    },{
        id: "4",
        image: "./images/image1.png",
        link: "#",
        alt: "images4"
    },{
        id: "5",
        image: "./images/image1.png",
        link: "#",
        alt: "images5"
    },{
        id: "6",
        image: "./images/image1.png",
        link: "#",
        alt: "images6"
    },
];

export const topProductsSmallSlider: Product[] = [
    {
        title: "همزن برقی sanford",
        image: "./images/image2.png",
        price: 125000,
        discount: 30
    },{
        title: "همزن برقی sanford",
        image: "./images/image2.png",
        price: 1000000,
        discount: 30
    },{
        title: "همزن برقی sanford",
        image: "./images/image2.png",
        price: 1000000,
        discount: 30
    },{
        title: "همزن برقی sanford",
        image: "./images/image2.png",
        price: 1000000,
        discount: 30
    },{
        title: "همزن برقی sanford",
        image: "./images/image2.png",
        price: 1000000,
        discount: 30
    }
];

export const middleBanner: {image: string, link: string}[] = [
    {
        image: "./images/image28.png",
        link: "#",
    },{
        image: "./images/image29.png",
        link: "#",
    }
];

export const middleBannerMobile: {image: string, link: string}[] = [
    {
        image: "./images/image78.png",
        link: "#",
    },{
        image: "./images/image78.png",
        link: "#",
    }
];


export const catalogBanner: {image: string, price: number, discount: number}[] = [
    {
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    },{
        image: "./images/image31.png",
        price: 125000,
        discount: 30
    }
];

export const catalogItems: {image: string, title: string}[] = [
    {
        image: "./images/image32.png",
        title: "مد و پوشاک"
    },{
        image: "./images/image100.png",
        title: "موبایل"
    },{
        image: "./images/image101.png",
        title: "کالای دیجیتال"
    },{
        image: "./images/image102.png",
        title: "لوارم تحریر"
    },{
        image: "./images/image103.png",
        title: "تجهیزات صنعتی"
    },{
        image: "./images/image104.png",
        title: "سوپرمارکت"
    },{
        image: "./images/image105.png",
        title: "اسباب بازی"
    },{
        image: "./images/image106.png",
        title: "زیبایی و سلامت"
    },{
        image: "./images/image107.png",
        title: "خانه و آشپزخانه"
    }
];

export const catalogItemsAmazing: Product[] = [
    {
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    },{
        title: "کتونی نایک شماره ۱۲۱",
        image: "./images/image30.png",
        price: 125000,
        discount: 30
    }
];

export const downloadItems: {image: string, text: string, link: string}[] = [
    {
        text: "دانلود از بازار",
        image: "./images/image8.png",
        link: "#"
    },{
        text: "دانلود از مایکت",
        image: "./images/image9.png",
        link: "#"
    },{
        text: "دانلود مستقیم",
        image: "./images/image10.png",
        link: "#"
    }
];

export const footerItems: {title: string, items: {text: string, link: string, icon?: JSX.Element}[]}[] = [
    {
        title: "با ویتسل",
        items: [
            {
                text: "درباره ما",
                link: "#"
            },{
                text: "تماس با ما",
                link: "#"
            },{
                text: "حریم خصوصی",
                link: "#"
            },{
                text: "شرایط بازگشت کالا",
                link: "#"
            }
        ]
    },{
        title: "محصولات ویتسل",
        items: [
            {
                text: "کالای دیجیتال",
                link: "#"
            },{
                text: "سوپرمارکت",
                link: "#"
            },{
                text: "گوشی موبایل",
                link: "#"
            },{
                text: "ابزار الات",
                link: "#"
            }
        ]
    },{
        title: "ارتباط با ویتسل",
        items: [
            {
                text: "ٰvitdell@gmail.com",
                link: "#",
                icon: <Send/>
            },{
                text: "۰۸۴۷۷۴۷۳۲۲",
                link: "#",
                icon: <Call/>
            },{
                text: "خیابان ولی عصرنرسیده به سینما آفریقا",
                link: "#",
                icon: <Map/>
            }
        ]
    }
];

export const footerImages: string[] = [
    "./images/image50.png",
    "./images/image51.png",
    "./images/image52.png",
    "./images/image53.png"
];

export const meta: MetaProps = {
    title: "فروشگاه اینترنتی ویتسل",
    description: "فروشگاه اینترنتی ویتسل",
    canonical: "فروشگاه اینترنتی ویتسل",
    url: "/",
}


