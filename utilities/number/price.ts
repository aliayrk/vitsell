export const priceSeprator = (price: number) => {
    if(!price) return 0;
    return price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}