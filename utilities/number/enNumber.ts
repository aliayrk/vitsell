export function convertToEnNumber(number: string) {
    if(number) {
        let txt = number + "";

        return txt.replace(/[٠١٢٣٤٥٦٧٨٩]/g,function (d: string){
            return `${d.charCodeAt(0) - 1632}`; // Convert Arabic numbers
        }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d: string) {
            return `${d.charCodeAt(0) - 1776}`; // Convert Persian numbers
        });
    }else {
        return number;
    }

}