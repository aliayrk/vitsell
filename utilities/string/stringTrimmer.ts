export const addSeparator = (number:number) => {
    if(!number) return "";
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export const excerpt = (string:string, length:number) => {
    if(string.length > length) {
        return string.slice(0, length) + ' ...';
    }else {
        return string;
    }
}

