export interface Product {
    image: string;
    price: number
    discount: number
    title: string
}