import React, {FunctionComponent, useEffect, useRef, useState} from 'react';
import styles from "./input.module.scss";
import {convertToEnNumber} from "@/utilities/number/enNumber";
import {addSeparator} from "@/utilities/string/stringTrimmer";

interface InputProps {
    id: string
    onChange: (event: { id: string, value: any }) => void
    onKeyDown?: (event: any) => void
    customClasses?: string
    lock?: boolean
    loading?: boolean
    icon?: JSX.Element
    placeholder?: string
    minLength?: number
    maxLength?: number
    type?: string
    defaultValue?: string
    loadingIcon?: JSX.Element
    align?: "center" | "left" | "right"
    extraIcons?: JSX.Element[]
    extraIconsClassName?: string[]
    focus?: boolean
    hasHint?:boolean
}

const Input : FunctionComponent<InputProps> = (props) => {

    let ref = useRef<HTMLInputElement | null>(null);

    const [defValue, setDefValue] = useState<string|null>(null);

    useEffect(()=> {
        if(props.defaultValue && props.type && props.type === "price") {
            let value = props.defaultValue.replaceAll(",","");
            value = convertToEnNumber(value);
            value = addSeparator(parseInt(value));

            setDefValue(value);
        }
    },[props.defaultValue, props.type])

    const handleChange = (event:any) => {
        setDefValue(null)
        let type = props.type ? props.type : "text";
        let value : any;

        switch (type) {
            case 'number':
                value = convertToEnNumber(event.target.value);
                value = value.replace(/[^0-9.]/g,'');
                break;
            case 'price':
                value = event.target.value;
                value = value.replaceAll(",","");
                value = convertToEnNumber(value);
                value = value.replace(/[^0-9.]/g,'');
                break;
            case 'text':
                value = event.target.value;
                break;
            case 'english':
                value = event.target.value;
                value = value.replace(/[^\x00-\x7F]/g, "");
                break;

            default:
                value = event.target.value;
                break;
        }
        let id = event.target.id;

        props.onChange({id: id, value: value});
    }

    const clickIcon = () => {
        let input = ref.current;
        if(!input) return;
        if(props.type === "password") {
            if(input.getAttribute("type") === "text") {
                input.setAttribute('type', 'password');
            }else {
                input.setAttribute('type', 'text');
            }
        }else {
            input.value = "";
        }

    }

    return (
        <>
            <input
                className={`
                    w-100 p-2 subText 
                    ${props.customClasses} 
                    ${styles.input} 
                    ${props.lock ? styles.disableSelect : ''}
                    ${props.hasHint ? styles.hintInput : ''}
                `}
                onChange={handleChange}
                onKeyDown={props.onKeyDown && props.onKeyDown}
                disabled={props.lock}
                maxLength={props.maxLength}
                autoFocus={props.focus}
                minLength={props.minLength}
                placeholder={props.placeholder}
                type={props.type === "number" ? "text" : props.type}
                value={defValue ? defValue : props.defaultValue}
                id={props.id}
                ref={ref}
                style={{textAlign: props.align ? props.align : "right"}}
            />
            {props.loading && <div className={styles.icon}>
                {props.loadingIcon && props.loadingIcon}
            </div>
            }
            {(props.icon && !props.loading) && <div
                className={styles.icon}
                onClick={clickIcon}
            >
                {props.icon && props.icon}
            </div>}
            {props.extraIcons && props.extraIcons.map((icon, index)=> {
                return(
                    <div
                        className={props.extraIconsClassName &&
                        props.extraIconsClassName[index] ?
                            props.extraIconsClassName[index] :
                            ''}
                        key={index}
                    >
                        {icon}
                    </div>
                )
            })
            }
        </>
    );
}

export default Input;