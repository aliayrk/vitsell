import React, {FunctionComponent} from 'react';
import style from "./productPreview.module.scss";
import {priceSeprator} from "@/utilities/number/price";
import {excerpt} from "@/utilities/string/stringTrimmer";
import useWidthChange from "../../../../hooks/useWidthChange";

interface ProductProps {
    image: string;
    title?: string;
    price: number;
    discount: number;
    className?: string;
    flag?: boolean
    imageRight?: boolean
}

const ProductPreviewTypeA : FunctionComponent<ProductProps> = (props) => {

    const { isMobile } = useWidthChange();

    return (
        <div className={`d-flex w-100 h-100 ${props.className} ${props.imageRight ? "flex-row" : "flex-column"} justify-content-center align-items-center ${style.background}`}>
            <div className={`${style.imageContainer} ${props.imageRight && style.img}`}>
                <img
                    src={props.image}
                    alt={"cover"}
                />
            </div>
            <div className={`d-flex w-100 h-100 flex-column ${props.imageRight ? "px-2" : "p-3"}`}>
                {props.title && <p className={`${style.title} mt-2 mb-2 w-100 fontV1`}>{excerpt(props.title, !isMobile ? 60 : 120)}</p>}
                <p className={`${style.price} fontV2 mt-2 mb-2 w-100`}>{priceSeprator(props.price)} <span>تومان</span></p>
                <div className={"d-flex flex-row-reverse flex-wrap align-items-center justify-content-between w-100"}>
                    <p className={`${style.oldPrice} fontV2 m-0`}>{priceSeprator(props.price)}</p>
                    <p className={`${style.percent} fontV1`}>{props.discount}%</p>
                </div>
            </div>
        </div>
    );
};

export default ProductPreviewTypeA;