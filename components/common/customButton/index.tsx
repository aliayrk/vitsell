import React, {FunctionComponent} from 'react';
import styles from "./customButton.module.scss";
import CmsColors from "../../types/CmsColors";
import Link from "next/link";

const theme = [
    "no-border",
    "white",
    "white-outline",
    "primary",
    "primary-outline",
    "secondary",
    "secondary-outline",
    "third",
    "third-outline",
    "light-gray",
    "blue",
    "blue-outline",
    "black",
    "black-outline",
    "green",
    "green-outline",
    "red",
    "red-outline",
    "confirm",
    "confirm-outline",
    "delete",
    "delete-outline",
    "disabled",
    "disabled-outline"
] as const;
type Theme = typeof theme[number];

interface BtnProps {
    href?: string
    disabled?: boolean
    loading?: boolean
    className?: string
    text?: string
    target?: string
    fullWidth?: boolean
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void
    theme?: Theme
    borderColor?:CmsColors
    textColor?:CmsColors
    textHOverColor?:CmsColors
    loadingTheme?: "black" | "white"
    style?: object
    title?: string
    icon?: JSX.Element
    loadingIcon?: JSX.Element
}

const CustomButton : FunctionComponent<BtnProps> = (props) => {

    const createChildrenElement=()=> {
        return(
            !props.loading ?
                <>
                    {props.icon && <div className={props.text ? "l-spacer-10":""}>
                        <div
                            className={`d-flex justify-content-center align-items-center`}
                            style={{width:30, height:30}}
                        >
                            {props.icon}
                        </div>
                    </div>}
                    {props.text && props.text}
                </>:
                <>
                    {props.icon && <div className={props.text ? "l-spacer-10":""}>
                        <div
                            className={`opacity-0 d-flex justify-content-center align-items-center`}
                            style={{width:30, height:30}}
                        >
                            {props.icon}
                        </div>
                    </div>}
                    <div className={"opacity-0"}>{props.text && props.text}</div>
                    <div className="position-absolute">{props.loadingIcon && props.loadingIcon}</div>
                </>
        )
    }

    return (
        <>
            {
                !props.href ? <button
                    className={`d-flex justify-content-center align-items-center mediumFont
                    ${styles.customBtn} 
                    ${props.theme ? styles["customBtn-"+props.theme] : ""} 
                    ${props.borderColor ? "cms-border-"+props.borderColor : ""} 
                    ${props.disabled ? styles["customBtn-disabled"] : "" } 
                    ${props.fullWidth ? "w-100" : "" }
                    ${props.textColor ? `cms-color-${props.textColor}` : "" }
                    ${props.textHOverColor ? `cms-hover-${props.textHOverColor}` : "" }
                    ${props.className}`
                    }
                    style={props.style}
                    title={props.title}
                    onClick={props.loading || props.disabled  ? ()=>{} : props.onClick}
                >
                    {createChildrenElement()}

                </button> : <Link href={props.href}><a
                    className={`d-flex justify-content-center align-items-center
                        ${styles.customBtn} 
                        ${props.theme ? styles["customBtn-"+props.theme] : styles["customBtn-primary-outline"]} 
                        ${props.borderColor ? styles["customBtnBorder-"+props.borderColor] : ""}
                        ${props.disabled ? styles["customBtn-disabled"] : "" } 
                        ${props.fullWidth ? "w-100" : "" }
                        ${props.className}`
                    }
                    style={props.style}
                    title={props.title}
                    target={props.target ? props.target : "_blank"}
                >
                    {createChildrenElement()}
                </a></Link>
            }
        </>

    );
};

export default CustomButton;