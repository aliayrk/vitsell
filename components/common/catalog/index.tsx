import React, {FunctionComponent} from 'react';
import styles from "./style.module.scss";
import TitleBox from "../titleBox";
import Icon from "../icon";
import useWidthChange from "../../../hooks/useWidthChange";

interface CatalogProps {
    list: {[key: string]: any}[]
    countOfCard?: number
    showLoadMore: boolean
    content: JSX.Element
    title: string
    iconTitle?: JSX.Element
    className?: string
    showItemInMobile?: number
}

const Catalog: FunctionComponent<CatalogProps> = (props) => {

    const { isMobile } = useWidthChange();

    return (
        <div className={`${styles.homeBoxRound} w-100 ${props.className ? props.className : ''}`}>
            <TitleBox
                className={"mb-2"}
                showLoadMore={!isMobile && props.showLoadMore}
                titleContent={<div className={"d-flex align-items-center"}>
                    {props.iconTitle && <div className={styles.bgIcon}><Icon size={24} color={"red"} icon={props.iconTitle}/></div>}
                    <p className={"text-nowrap fontV1 r-spacer-10"}>{props.title}</p>
                </div>}
                text={!isMobile ? "مشاهده همه" : undefined}
            />
            <div>
                {props.content}
            </div>
        </div>
    )
};

export default Catalog;
