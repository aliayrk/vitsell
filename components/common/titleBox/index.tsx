import React, {FunctionComponent} from 'react';
import styles from "./style.module.scss";
import CustomButton from "../customButton";
import Icon from "@/components/common/icon";
import {Left} from "@/components/common/svgIcons";

interface TitleBoxProps {
    showLoadMore: boolean
    titleContent: JSX.Element
    className?: string
    text?: string
}

const TitleBox: FunctionComponent<TitleBoxProps> = (props) => {

    return <div className={`d-flex align-items-center justify-content-between ${styles.titleMargin} ${props.className}`}>
        {props.titleContent}
        <div className={styles.line}/>
        {props.showLoadMore && <CustomButton
            text={props.text ? props.text : ""}
            icon={<Icon size={5} color={"black"} icon={<Left/>}/>}
            theme={"no-border"}
            className={styles.loadMoreButton}
        />}
    </div>
};

export default TitleBox;
