import React, {FunctionComponent} from 'react';
import styles from "./card.module.scss";
import cmsColors from "../../types/CmsColors";
import Link from "next/link";

interface CardProps {
    url?: string;
    className?: string;
    animation?: boolean;
    border?: number;
    borderColor?: cmsColors;
    shadow?: boolean;
}

const Card : FunctionComponent<CardProps> = (props) => {

    return (
        <>
        {
            props.url ?
                <Link href={props.url} >
                <a
                    className={`
                        ${styles.box}
                        ${props.className}
                        ${styles.card}
                        ${props.animation && styles.cardAnimation}
                        ${props.shadow && styles.cardShadow}
                        cornerStyle-small
                    `}
                    style={{borderWidth: props.border, borderColor: props.borderColor}}
                >
                    {props.children}
                </a>
             </Link>
            :
                <a
                    className={`
                        ${styles.box}
                        ${props.className}
                        ${styles.card}
                        ${props.animation && styles.cardAnimation}
                        ${props.shadow && styles.cardShadow}
                        cornerStyle-small
                    `}
                    style={{borderWidth: props.border, borderColor: props.borderColor}}
                >
                    {props.children}
                </a>
        }
        </>
    );
};

export default Card;