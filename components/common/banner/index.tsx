import React, {FunctionComponent} from 'react';
import {Col, Row} from "react-bootstrap";
import Card from "../card";

export interface BannerProps {
    sizes: number[]
    sources: {image: string, link: string}[]
    className?: string
    column?: boolean
}

const Banner:FunctionComponent<BannerProps> = (props) => {

    return (
        <Row className={props.className ? props.className : ''}>
            {
                props.sources.map((source, index) => (
                    <Col className={"mb-2"} key={index} md={props.sizes[index]} sm={props.column ? 12 : props.sizes[index]} xs={props.column ? 12 : props.sizes[index]}>
                        <Card url={source.link}>
                            <img
                                src={source.image}
                                alt={"cover"}
                                style={{
                                    width: '100%',
                                    // height: '100%',
                                    borderRadius: 16
                                }}
                            />
                        </Card>
                    </Col>
                ))
            }
        </Row>
    );
};

export default Banner;