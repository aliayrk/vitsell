import React, {FunctionComponent} from 'react';
import styles from "./style.module.scss";
import Icon from "../../../common/icon";
import {ArrowRight} from "../../../common/svgIcons";
import cmsColor from "../../../types/CmsColors";

interface SliderArrowIconsProps {
    arrow: "left" | "right";
    size?: number
    color?: cmsColor
    className?: string
}
const SliderArrowIcons: FunctionComponent<SliderArrowIconsProps> = (props) => {

    return <div className={`${props.className}`}>
        <div>
            {props.arrow === "left" ? <div className={"position-relative d-flex align-items-center justify-content-center"}>
                <img alt={"left"} src={"./icons/arrowContainer.png"} />
                <Icon size={18} color={"black"} icon={<ArrowRight/>} className={`${styles.arrowIcon} ${styles.rotate}`}/>
            </div> : <div className={"d-flex align-items-center justify-content-center"}>
                <img alt={"right"} src={"./icons/arrowContainer.png"} className={styles.rotate}/>
                <Icon size={18} color={"black"} icon={<ArrowRight/>} className={styles.arrowIcon}/>
            </div>}
        </div>
    </div>
};

export default SliderArrowIcons;
