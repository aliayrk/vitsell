import React, {FunctionComponent, useState} from 'react';
import Slider_slick from "react-slick";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styles from "./slider.module.scss";
import CmsColors from "../../types/CmsColors";

interface SubMenuProps {
    initialCol: number;
    autoPlay?: boolean;
    dots?: boolean;
    arrows?: boolean;
    list: {[key: string]: any}[];
    nextIcon?: JSX.Element;
    previousIcon?: JSX.Element;
    showItemInMobile?: number;
    centerMode?: boolean;
    swipeToSlide?: boolean;
    focusOnSelect?: boolean;
    dotsInView?: boolean;
    navFor?:Slider_slick;
    setSlide?:(slider:Slider_slick|null)=>void;
    bgArrows?: CmsColors;
    arrowsBorderRadius?: number;
    dotVertical?: boolean
    className?: string
}
const Slider: FunctionComponent<SubMenuProps> = (props) => {

    const SampleNextArrow = (propsButton: { className?: any; style?: any; onClick?: any; }) => {
        const { className, style, onClick } = propsButton;
        return (
            <div
                className={`${className} ${styles.next} slick-arrow`}
                style={{
                    ...style,
                    display: "flex",
                    borderRadius: props.arrowsBorderRadius ? props.arrowsBorderRadius : 0,
                    backgroundColor: props.bgArrows ? props.bgArrows : "cms-back-white"
                }}
                onClick={onClick}
            >
                {props.nextIcon && props.nextIcon}
            </div>
        );
    }

    const SamplePrevArrow = (propsButton: { className?: any; style?: any; onClick?: any; }) => {
        const { className, style, onClick } = propsButton;
        return (
            <div
                className={`${className} ${styles.pre} slick-arrow`}
                style={{
                    ...style,
                    display: "flex",
                    borderRadius: props.arrowsBorderRadius ? props.arrowsBorderRadius : 0,
                    backgroundColor: props.bgArrows ? props.bgArrows : "cms-back-white"
                }}
                onClick={onClick}
            >
                {props.previousIcon && props.previousIcon}
            </div>
        );
    }

    let defaultSettings: {[key: string]: any} = {
        infinite: true,
        speed: 500,
        draggable: true,
        centerPadding: "0px",
        slidesToScroll: 1,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />
    };
    let settings : {[key: string]: any} = {... defaultSettings};

    settings.responsive = [
        {
            breakpoint: 1920,
            settings: {
                slidesToShow: props.initialCol ? props.initialCol : 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: props.initialCol ? props.initialCol : 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 750,
            settings: {
                slidesToShow: props.initialCol ? props.initialCol : 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 576,
            settings: {
                slidesToShow: props.showItemInMobile ? props.showItemInMobile : 1,
                slidesToScroll: 1
            }
        }
    ];
    settings.slidesToShow = props.initialCol;
    settings.dots = !!props.dots;
    settings.arrows = !!props.arrows;
    // settings.centerMode = !!props.centerMode;
    settings.swipeToSlide = !!props.swipeToSlide;
    settings.focusOnSelect = !!props.focusOnSelect;
    // settings.variableWidth = true;

    if(props.autoPlay) {
        settings.autoplay = true;
        settings.autoplaySpeed = 3000;
        settings.pauseOnHover = true;
    } else {
        settings.autoplay = false;
        settings.autoplaySpeed = 0;
    }

    const [isSlider, setIsSlider] = useState<boolean>(true);

    return (
        <div className={`${styles.sliderContainer} ${styles.dotsInView} ${props.className}`}>
            <div className={styles.sliderRow}>
                {
                    !isSlider ?
                        <div className={`${styles.dataRow} px-0 justify-content-start`}>
                            {props.children}
                        </div> :
                        <Slider_slick
                            {... settings}
                            asNavFor={props.navFor}
                            dotsClass={`${styles.dot} ${props.dotVertical && styles.dotVertical}`}
                            ref={slider=>props.setSlide && props.setSlide(slider)}
                        >
                            {props.children}
                        </Slider_slick>
                }
            </div>
        </div>
    );
}

export default Slider;