import React, {FunctionComponent} from 'react';
import styles from "./style.module.scss";
import Slider from "../../../common/slider";
import SliderArrowIcons from "../../../common/slider/sliderArrowIcons";
import Card from "../../../common/card";
import {Banner} from "@/services/models/Banner";

interface MainBannerProps {
    banner: Banner[]
}

const MainBanner: FunctionComponent<MainBannerProps> = (props) => {

    return <div className={styles.sliderContainer}>
        <Slider
            initialCol={1}
            list={props.banner}
            showItemInMobile={1}
            className={"h-100"}
            focusOnSelect
            autoPlay
            arrows={true}
            nextIcon={<SliderArrowIcons arrow={"left"} />}
            previousIcon={<SliderArrowIcons arrow={"right"} />}
            bgArrows={"transparent"}
        >
            {
                props.banner.map((banner, index)=> {
                    return(
                        <Card
                            key={index}
                            url={banner.link}
                            className={styles.card}
                        >
                            <img
                                src={banner.image}
                                alt={banner.alt}
                            />
                        </Card>
                    )
                })
            }
        </Slider>
    </div>
}

export default MainBanner;
