import React, {FunctionComponent} from 'react';
import styles from "./style.module.scss";
import useWidthChange from "../../../../hooks/useWidthChange";
import CustomButton from "@/components/common/customButton";
import {downloadItems} from "@/constants/home";

const DownloadContainer: FunctionComponent = () => {

    const { isMobile } = useWidthChange();

    return <div className={`${!isMobile && styles.paddingContainer}`}>
        <div className={`${styles.bg} position-relative d-flex flex-column p-4`}>
            <p className={"fontV1 mb-4"} style={{color: 'white'}}>ویتسل را همیشه همراه داشته باشید!</p>
            <div className={`d-flex flex-wrap ${!isMobile && "w-50"}`}>
                {downloadItems.map((item, index) => {
                    return(
                        <CustomButton
                            key={index}
                            theme={"light-gray"}
                            className={`${styles.btnDownload} l-spacer-10 flex-fill ${index !== downloadItems.length - 1 && "mb-4"}`}
                            text={item.text}
                            href={item.link}
                            icon={<img src={item.image} alt={item.text} style={{width: 18}}/>}
                        />
                    )
                })}
            </div>
            <img src={"./images/mobile.png"} alt={"mobile"} className={`${styles.mobile} ${isMobile && "d-none"}`}/>
        </div>
    </div>
}

export default DownloadContainer;
