import React, {FunctionComponent} from 'react';
import styles from "./style.module.scss";
import Slider from "../../../common/slider";
import SliderArrowIcons from "../../../common/slider/sliderArrowIcons";
import useWidthChange from "../../../../hooks/useWidthChange";
import Card from "@/components/common/card";
import ProductPreviewTypeA from "@/components/common/productPreview/typeA";
import {Product} from "@/services/models/Product";

interface ProductsBannerProps {
    products: Product[]
}

const ProductsBanner: FunctionComponent<ProductsBannerProps> = (props) => {

    const { isMobile } = useWidthChange();

    return <div className={styles.sliderContainer}>
        <Slider
            initialCol={1}
            list={props.products}
            showItemInMobile={1}
            dots={false}
            focusOnSelect
            autoPlay
            className={"h-100"}
            arrows={!isMobile}
            nextIcon={<SliderArrowIcons arrow={"left"}/>}
            previousIcon={<SliderArrowIcons arrow={"right"}/>}
            bgArrows={"transparent"}
        >
            {
                props.products.map((product, index)=> {
                    return(
                        <Card
                            key={index}
                            className={styles.card}
                        >
                            <ProductPreviewTypeA
                                image={product.image}
                                price={product.price}
                                discount={product.discount}
                                title={product.title}
                            />
                        </Card>
                    )
                })
            }
        </Slider>
    </div>
};

export default ProductsBanner;
