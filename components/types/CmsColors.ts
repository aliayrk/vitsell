const color = [
    "primary",
    "secondary",
    "third",
    "light-gray",
    "mid-gray",
    "dark-gray",
    "white",
    "black",
    "red",
    "green",
    "blue",
    "yellow",
    "transparent",
    "text-color"

] as const;

type Color = typeof color[number];
export default Color;