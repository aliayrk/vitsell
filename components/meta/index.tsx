import React, {FunctionComponent} from 'react';
import Head from 'next/head';

export interface MetaProps {
    title: string;
    description: string;
    canonical: string;
    image?: string
    url: string;
    keyword?: string;
}

const Meta: FunctionComponent<MetaProps> = (props) => {
    return (
        <Head>
            <title>{props.title}</title>
            <meta name="description" content={props.description}/>
            <link rel="canonical" href={props.canonical}/>

            <link href={"/favicon.ico"} rel="apple-touch-icon" sizes="180x180"/>
            <link href={"/favicon.ico"} rel="shortcut icon"/>

            <meta property="og:title" content={props.title}/>
            <meta property="og:image" content={props.image}/>
            <meta property="og:description" content={props.description}/>
            <meta property="og:url" content={props.url}/>
        </Head>
    );
};

export default Meta;