import React, {FunctionComponent} from 'react';
import Header from "./header";
import styles from "./style.module.scss";
import Footer from "./footer";
import {HEADER_DESKTOP_HEIGHT} from '@/config';

const Layout:FunctionComponent = (props) => {

    return (<main className={`${styles.main} light-theme`} id="main">

            <div
                className={styles.header}
                style={{
                    height: HEADER_DESKTOP_HEIGHT,
                }}
            >
                <Header />
            </div>

            <div
                className={styles.containerLayout}
            >
                <div
                    className={styles.content}
                >
                    {props.children}
                </div>
                <div className={styles.footer}>
                    <Footer />
                </div>
            </div>
        </main>
    );
};

export default Layout;