import React, {FunctionComponent} from 'react';
import styles from "./header.module.scss";
import useWidthChange from "../../../hooks/useWidthChange";
import CustomButton from "@/components/common/customButton";
import Icon from "@/components/common/icon";
import {Category, Discount, Hot, Login, Menu, Search, Shopping} from "@/components/common/svgIcons";
import Input from "@/components/common/form/formElement/input";
import MainContainer from "@/components/layout/containers/mainContainer";

const Header: FunctionComponent = () => {

    const { isMobile } = useWidthChange();

    return (
        <MainContainer>
        <div className={`w-100 d-flex align-items-center ${styles.header}`}>
                <div className={"w-100 d-flex align-items-center justify-content-between"}>
                        {isMobile ?
                            <CustomButton
                                icon={<Icon size={24} color={"black"} icon={<Menu/>}/>}
                                theme={"no-border"}
                                className={"px-0"}
                            /> :
                            <div className={"d-flex align-items-center"}>
                                <img
                                    alt={"logo"}
                                    src={"./images/Logo.png"}
                                    style={{width: 40, objectFit: "contain"}}
                                />
                                <CustomButton
                                    icon={<Icon size={18} color={"black"} icon={<Category/>}/>}
                                    text={"دسته بندی ها"}
                                    theme={"no-border"}
                                /><CustomButton
                                    icon={<Icon size={18} color={"black"} icon={<Hot/>}/>}
                                    text={"پرفروش ترین ها"}
                                    theme={"no-border"}
                            /><CustomButton
                                    icon={<Icon size={18} color={"black"} icon={<Discount/>}/>}
                                    text={"تخفیف دار ها"}
                                    theme={"no-border"}
                            />
                            </div>
                        }
                        <div className={"d-flex"}>
                            <div className={"position-relative"}>
                                <Input
                                    id={"search"}
                                    onChange={()=>{}}
                                    icon={<Icon size={18} color={"black"} icon={<Search/>}/>}
                                    customClasses={styles.search}
                                    placeholder={isMobile ? "جستجو..." : "جستجوی محصولات ..."}
                                />
                            </div>
                            <CustomButton
                                icon={<Icon size={20} color={"black"} icon={<Shopping/>}/>}
                                theme={!isMobile ? "light-gray" : "no-border"}
                                className={`border-0 ${!isMobile && "mx-2"}`}
                            />
                            <CustomButton
                                icon={isMobile ? <Icon size={18} color={"black"} icon={<Login/>}/> : undefined}
                                text={!isMobile ? "ورود/ثبت نام" : undefined}
                                theme={isMobile ? "no-border" : "red"}
                                className={`text-nowrap ${styles.authBtn}`}
                            />
                        </div>
                </div>
        </div>
        </MainContainer>
    )
};

export default Header;
