import React, {FunctionComponent} from 'react';
import styles from "./footer.module.scss";
import MainContainer from "../../layout/containers/mainContainer";
import {Col, Row} from "react-bootstrap";
import {footerImages, footerItems} from "@/constants/home";
import Link from "next/link";
import useWidthChange from "@/hooks/useWidthChange";
import Icon from "@/components/common/icon";
import CustomButton from "@/components/common/customButton";
import {Instagram, Send2, Whatsapp} from "@/components/common/svgIcons";

const Footer: FunctionComponent = () => {

    const { isMobile } = useWidthChange();

    return <div className={`d-flex flex-column pt-4`}>
        <MainContainer>
            <Row>
                <Col md={10} sm={12} xs={12}>
                    <Row>
                        <Col sm={0} xs={0} className={`${isMobile && "d-none"} d-flex flex-column`}>
                            <img src={"./images/Logo.png"} alt={"logo"} style={{width: 100}}/>
                            <p className={"fontV1 my-2"}>ویتسل</p>
                            <p>صرافی هفت ارز پلتفرم ایمن و سریع برای انجام معاملات ارز دیجیتال در بستری مطمئن</p>
                        </Col>
                        {
                            footerItems.map((footer, index)=>{
                                return(
                                    <Col key={index} md={isMobile ? 4 : 3} sm={6} xs={6} className={`${isMobile && "mb-3"} d-flex flex-column`}>
                                        <p className={"fontV1 mb-4"}>{footer.title}</p>
                                        {
                                            footer.items.map((item, indexJ)=>{
                                                return <div className={"d-flex mb-3"}>
                                                    {item.icon && <Icon size={22} color={indexJ === 0 ? "white" : "black"} icon={item.icon} className={"l-spacer-10"}/>}
                                                        <Link href={item.link}>
                                                            <a key={indexJ}>{item.text}</a>
                                                        </Link>
                                                </div>
                                            })
                                        }
                                        {
                                            index === footerItems.length - 1 && <div className={"d-flex"}>
                                                <CustomButton
                                                    icon={<Icon size={24} color={"black"} icon={<Instagram/>}/>}
                                                    theme={"light-gray"}
                                                    className={styles.round}
                                                />
                                                <CustomButton
                                                    icon={<Icon size={24} color={"black"} icon={<Send2/>}/>}
                                                    theme={"light-gray"}
                                                    className={`mx-2 ${styles.round}`}
                                                />
                                                <CustomButton
                                                    icon={<Icon size={24} color={"black"} icon={<Whatsapp/>}/>}
                                                    theme={"light-gray"}
                                                    className={styles.round}
                                                />
                                            </div>
                                        }
                                    </Col>
                                )
                            })
                        }
                    </Row>
                </Col>
                <Col md={2} sm={12} xs={12} className={isMobile ? "d-flex flex-row" : `${styles.namad} d-flex flex-column`}>
                    {
                        footerImages.map((img, index)=>{
                            if(!isMobile && ![0,1].includes(index)) { return }
                            return(<div key={index} className={styles.imgContainer}>
                                <img src={img} alt={"img"}/>
                            </div>)
                        })
                    }
                </Col>
                <Col md={12} sm={12} xs={12} className={styles.line}>
                    <p className={"w-100 text-right"}>کلیه حقوق متعلق به شرکت ویستا است.</p>
                </Col>
            </Row>
        </MainContainer>
    </div>
};

export default Footer;
