import React, {FunctionComponent} from 'react';
import styles from "./mainContainer.module.scss";

interface MainContainer {
    background?:string
}

const MainContainer: FunctionComponent <MainContainer> = (props) => {
    return (
        <div
            className={styles.mainContainer}
            style={{background:props.background}}
        >
            {props.children}
        </div>
    );
};

export default MainContainer;