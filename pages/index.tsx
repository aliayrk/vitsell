import type {NextPage} from 'next'
import React from "react";
import MainBanner from "@/components/pages/home/mainBanner";
import {
    catalogBanner,
    catalogItems, catalogItemsAmazing, meta,
    middleBanner,
    middleBannerMobile,
    topBannerSlider,
    topProductsSmallSlider
} from "@/constants/home";
import {Col, Row} from "react-bootstrap";
import MainContainer from "@/components/layout/containers/mainContainer";
import Catalog from "@/components/common/catalog";
import Banner from "@/components/common/banner";
import ProductsBanner from "@/components/pages/home/produtBanner";
import {Category, Discount, Hot, Star} from "@/components/common/svgIcons";
import Card from "@/components/common/card";
import styles from "../styles/Home.module.scss";
import ProductPreviewTypeA from "@/components/common/productPreview/typeA";
import SliderArrowIcons from "@/components/common/slider/sliderArrowIcons";
import Slider from "@/components/common/slider";
import DownloadContainer from "@/components/pages/home/download";
import useWidthChange from "@/hooks/useWidthChange";
import Meta from "@/components/meta";


const Home: NextPage = () => {

    const { isMobile } = useWidthChange();

    return (
        <div>
            <Meta
                title={meta.title}
                description={meta.description}
                canonical={meta.canonical}
                url={meta.url}
            />
            <MainContainer>
                <Row className={"mb-4"}>
                    <Col md={9} sm={12} xs={12}>
                        <MainBanner banner={topBannerSlider} />
                    </Col>
                    <Col md={3} className={isMobile ? "d-none" : ""}>
                        <ProductsBanner
                            products={topProductsSmallSlider}
                        />
                    </Col>
                </Row>
                <Banner
                    className={"mb-4"}
                    sizes={[6,6]}
                    sources={isMobile ? middleBannerMobile : middleBanner.map((
                        {
                            image,
                            link
                        }) => (
                        {
                            image: image,
                            link: link
                        }
                    ))}
                />
                <Catalog
                    list={catalogBanner}
                    title={"تخفیف های ویتسل"}
                    countOfCard={6.5}
                    iconTitle={<Discount/>}
                    showLoadMore={true}
                    content={<Slider
                        initialCol={6.5}
                        // centerMode={true}
                        list={catalogBanner}
                        showItemInMobile={2}
                        arrows
                        nextIcon={<SliderArrowIcons arrow={"left"} />}
                        previousIcon={<SliderArrowIcons arrow={"right"} />}
                        bgArrows={"transparent"}
                    >
                        {catalogBanner.map((product, index)=> {
                            return(
                                <Card key={index} shadow={false} className={styles.catalogBanner}>
                                    <ProductPreviewTypeA
                                        {...product}
                                    />
                                </Card>
                            )
                        })}
                    </Slider>}
                />
                <Banner
                    className={"my-3"}
                    sizes={[6,6]}
                    column
                    sources={middleBanner.reverse().map((
                        {
                            image,
                            link
                        }) => (
                        {
                            image: image,
                            link: link
                        }
                    ))}
                />
                <Catalog
                    list={catalogItems}
                    title={"دسته بندی های محبوب"}
                    countOfCard={6.5}
                    iconTitle={<Category/>}
                    showLoadMore={true}
                    content={
                        <Row>
                            <Col md={8} sm={12} xs={12}>
                                <Row className={'row-cols-lg-5'}>
                                    {catalogItems.map((item, index) => {
                                        return(
                                            <Col sm={4} xs={4} key={index} className={styles.catalogItemContainer}>
                                                <div className={styles.catalogItem}>
                                                    <img className={"w-100"} src={item.image} alt={"cover"}/>
                                                    <p className={"title text-center w-100"}>{item.title}</p>
                                                </div>
                                            </Col>
                                        )
                                    })}

                                </Row>
                            </Col>
                            <Col md={4} sm={0} xs={0} className={isMobile ? "d-none" : ""}>
                                <img className={"w-100"} alt={"cover"} src={"./images/image22.png"}/>
                            </Col>
                    </Row>}
                />
                <Catalog
                    list={catalogItems}
                    title={"پیشنهادات شگفت انگیز"}
                    countOfCard={6.5}
                    iconTitle={<Hot/>}
                    showLoadMore={true}
                    content={
                        <Row>
                            <Col md={3} sm={0} xs={0} className={isMobile ? "d-none" : "pb-3"}>
                                <ProductPreviewTypeA
                                    image={"./images/image2.png"}
                                    title={"همزن برقی sanford"}
                                    price={1200000}
                                    discount={24}
                                />
                            </Col>
                            <Col md={9} sm={12} xs={12}>
                                {
                                    !isMobile ? <Row className={'row-cols-lg-3'}>
                                        {catalogItemsAmazing.map((item, index) => {
                                            return(
                                                <Col key={index} className={styles.catalogItemContainer}>
                                                    <Card>
                                                        <ProductPreviewTypeA
                                                            {...item}
                                                            imageRight={true}
                                                        />
                                                    </Card>
                                                </Col>
                                            )
                                        })}
                                    </Row> :
                                        <Slider
                                            initialCol={6.5}
                                            list={catalogItemsAmazing}
                                            showItemInMobile={1.5}
                                            arrows={false}
                                            swipeToSlide={true}
                                            nextIcon={<SliderArrowIcons arrow={"left"} />}
                                            previousIcon={<SliderArrowIcons arrow={"right"} />}
                                            bgArrows={"transparent"}
                                        >
                                            {catalogItemsAmazing.map((item, index)=> {
                                                return(
                                                    <Card key={index} className={"px-1"}>
                                                        <ProductPreviewTypeA
                                                            {...item}
                                                            className={"p-1"}
                                                            imageRight={true}
                                                        />
                                                    </Card>
                                                )
                                            })}
                                        </Slider>
                                }
                            </Col>
                    </Row>}
                />
                <Catalog
                    className={"my-4"}
                    list={catalogBanner}
                    title={"پرفروض ترین محصولات"}
                    countOfCard={6.5}
                    iconTitle={<Star/>}
                    showLoadMore={true}
                    content={<Slider
                        initialCol={6.5}
                        // centerMode={true}
                        list={catalogBanner}
                        showItemInMobile={2}
                        arrows
                        nextIcon={<SliderArrowIcons arrow={"left"} />}
                        previousIcon={<SliderArrowIcons arrow={"right"} />}
                        bgArrows={"transparent"}
                    >
                        {catalogBanner.map((product, index)=> {
                            return(
                                <Card key={index} shadow={false} className={styles.catalogBanner}>
                                    <ProductPreviewTypeA
                                        {...product}
                                    />
                                </Card>
                            )
                        })}
                    </Slider>}
                />
                <Row className={"mb-2"}>
                    <Col md={6} sm={12} xs={12} className={"mb-3"}>
                        <img src={"./images/image40.png"} alt={"cover"} className={styles.imgContainer}/>
                    </Col>
                    <Col md={6} sm={12} xs={12} className={"mb-3"}>
                        <Row>
                            <Col md={12} sm={12} xs={12} className={"pb-2"}>
                                <img src={"./images/image41.png"} alt={"cover"} className={styles.imgContainer}/>
                            </Col>
                            <Col md={12} sm={12} xs={12} className={"pt-2"}>
                                <img src={"./images/image42.png"} alt={"cover"} className={styles.imgContainer}/>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <DownloadContainer />
            </MainContainer>
        </div>
    )
};

export default Home;
