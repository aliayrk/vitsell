import 'bootstrap/dist/css/bootstrap.min.css';
import type { AppProps } from 'next/app';
import Layout from '../components/layout';
import '../styles/styles.scss';

function MyApp({ Component, pageProps }: AppProps) {

  return <Layout {...pageProps} >
    <Component {...pageProps} />
  </Layout>
}

export default MyApp
